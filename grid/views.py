#!/usr/bin/env python

""" Views """

from flask import abort, jsonify, make_response, request
from grid.models import Grid
from grid.utils import check_win

TTT_MARKS = ['x', 'o']


def api_root():
    """
    Display API root message
    :return response: api root message
    :rtype app: HTTP response
    """
    response = jsonify('grid-api root')
    response.status_code = 200
    return response


def get_all_grid():
    """
    List all grids
    :return response: list of all grids with return code
    :rtype app: HTTP response
    """
    grids = Grid.get_all()
    results = []
    for grid in grids:
        results.append(grid.get_data())
    response = jsonify(results)
    response.status_code = 200
    return response


def get_grid(grid_id):
    """
    Get data about one grid
    :param grid_id: id of the grid
    :type grid_id: int
    :return response: details about one grid with return code
    :rtype app: HTTP response
    """
    grid = Grid.query.filter_by(id=grid_id).first()
    if not grid:
        abort(404)
    response = jsonify(grid.get_data())
    response.status_code = 200
    return response


def post_grid():
    """
    Create a new grid
    :return response: details about new grid created
    :rtype app: HTTP response
    """
    grid = Grid()
    grid.save()
    response = jsonify(grid.get_data())
    response.status_code = 201
    return response


def put_grid(grid_id):
    """
    Update a grid (make a play)
    :param grid_id: id of the grid
    :type grid_id: int
    :return response: details about updated grid
    :rtype app: HTTP response
    """
    grid = Grid.query.filter_by(id=grid_id).first()
    if not grid:
        abort(404)

    # Check if the grid is already finished
    if grid.winner or all([getattr(grid, 'square_{}'.format(i)) for i in range(9)]):
        abort(make_response("Grid already finished, you can't play", 403))

    # Check if square and mark are correctly used
    square = int(request.data.get('square', None))
    square_numbers = [i for i in range(9)]
    mark = request.data.get('mark', None)
    if not (square is not None and square in square_numbers):
        abort(make_response("Data wrong or missing: 'square' with values in {}".format(square_numbers),
                            400))
    if not (mark and mark in TTT_MARKS):
        abort(make_response("Data wrong or missing: 'mark' with values in {}".format(TTT_MARKS),
                            400))

    # Check if the current player isn't the last player
    if grid.last_player and grid.last_player == mark:
        abort(make_response("Wait for other player turn", 403))

    # Check if square is already setted
    square_current = getattr(grid, 'square_{}'.format(square))
    if square_current:
        abort(make_response("Square already setted to '{}'".format(square_current),
                            409))

    # Make the play
    setattr(grid, 'square_{}'.format(square), mark)
    grid.last_player = mark

    if check_win([grid.square_0, grid.square_1, grid.square_2,
                  grid.square_3, grid.square_4, grid.square_5,
                  grid.square_6, grid.square_7, grid.square_8],
                 mark):
        grid.winner = mark

    grid.save()
    response = jsonify(grid.get_data())
    response.status_code = 200
    return response


def delete_grid(grid_id):
    """
    Delete a grid
    :param grid_id: id of the grid
    :type grid_id: int
    :return response: message and status code
    :rtype app: HTTP response
    """
    grid = Grid.query.filter_by(id=grid_id).first()
    if not grid:
        abort(404)
    grid.delete()
    response = make_response("grid {} deleted successfully".format(grid.id),
                             200)
    return response
