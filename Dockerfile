FROM python:3.9-alpine

LABEL maintainer="thomasboni13@gmail.com"

# Gunciron
ENV PORT 5000
ENV HOST "0.0.0.0"

WORKDIR /grid/

COPY grid ./grid
COPY migrations ./migrations
COPY docker-entrypoint.sh Pipfile manage.py ./

RUN apk update && apk upgrade && apk add --no-cache \
    bash \
    gcc \
    postgresql-dev \
    musl-dev

RUN pip install pipenv && \
    pipenv --bare install && \
    chmod a+x ./docker-entrypoint.sh

EXPOSE 5000

ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["pipenv", "run", "gunicorn", "--bind", "${HOST}:${PORT}", "grid.grid:app"]
